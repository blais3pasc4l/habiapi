import boto3

dynamodb = boto3.resource('dynamodb')
table_name = 'VisitasTable'
table = dynamodb.Table(table_name)

def create_visita(visita_id, fecha, lugar):
    item = {
        'id': visita_id,
        'fecha': fecha,
        'lugar': lugar
    }
    table.put_item(Item=item)
    print(f"Visita {visita_id} creada exitosamente")

def get_visita(visita_id):
    response = table.get_item(Key={'id': visita_id})
    item = response.get('Item')
    if item:
        print("Información de la visita:")
        print(f"ID: {item['id']}")
        print(f"Fecha: {item['fecha']}")
        print(f"Lugar: {item['lugar']}")
    else:
        print(f"No se encontró la visita con ID {visita_id}")

def update_visita(visita_id, fecha=None, lugar=None):
    expression_attribute_values = {}
    update_expression = "set"
    
    if fecha:
        expression_attribute_values[':f'] = fecha
        update_expression += " fecha = :f,"
    
    if lugar:
        expression_attribute_values[':l'] = lugar
        update_expression += " lugar = :l,"

    update_expression = update_expression[:-1]  # Eliminar la coma final
    table.update_item(
        Key={'id': visita_id},
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_attribute_values
    )
    print(f"Visita {visita_id} actualizada exitosamente")

def delete_visita(visita_id):
    table.delete_item(Key={'id': visita_id})
    print(f"Visita {visita_id} eliminada exitosamente")