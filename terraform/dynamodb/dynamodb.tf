resource "aws_dynamodb_table" "visits" {
  name         = "VisitsTable"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "id"

  attribute {
    name = "id"
    type = "S"
  }
  attribute {
    name = "date"
    type = "S"
  }
  attribute {
    name = "location"
    type = "S"
  }
}

