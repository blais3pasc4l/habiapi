module "iam" {
  source = "./modules/iam"
  APPLICATION_NAME = var.GLOBAL_APPLICATION_NAME

  IAM_ROLE_ARN = var.IAM_ROLE_ARN
}







resource "aws_lambda_permission" "apigw_lambda_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test-lambda-function.arn
  principal     = "apigateway.amazonaws.com"

  source_arn = aws_api_gateway_rest_api.api.execution_arn
}